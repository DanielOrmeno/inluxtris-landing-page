<!Doctype html>
<head>
  <title>Inluxtris Pty Ltd</title>
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
  <link href="css/custom-theme.css" rel="stylesheet" type="text/css">
  <!-- Google's JQuery CDN -->
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!-- FullPage plugin -->
  <link rel="stylesheet" type="text/css" href="fpPlugin/jquery.fullPage.css" />
  <script type="text/javascript" src="fpPlugin/jquery.fullPage.js"></script>
  <!-- JQuery doc ready script -->
  <script type="text/javascript">
    $(document).ready(function() {
      //- FullPage plugin
      $('#fullpage').fullpage({
        //Navigation
        menu: false,
        anchors:['comingSoon', 'Contact'],
        navigation: false,
        navigationPosition: 'right',
        navigationTooltips: ['comingSoon', 'Contact'],
        showActiveTooltips: false,
        slidesNavigation: true,
        slidesNavPosition: 'bottom',

        //Scrolling
        css3: true,
        scrollingSpeed: 700,
        autoScrolling: true,
        fitToSection: true,
        scrollBar: false,
        easing: 'easeInOutCubic',
        easingcss3: 'ease',
        loopBottom: false,
        loopTop: false,
        loopHorizontal: true,
        continuousVertical: false,
        //normalScrollElements: '#element1, .element2',
        scrollOverflow: false,
        touchSensitivity: 15,
        normalScrollElementTouchThreshold: 5,

        //Accessibility
        keyboardScrolling: true,
        animateAnchor: true,
        recordHistory: true,

        //Design
        controlArrows: true,
        verticalCentered: true,
        resize : false,
        sectionsColor : ['#03A9F4', '#03A9F4'],
        paddingTop: '3em',
        paddingBottom: '10px',
        fixedElements: '#header, .footer',
        responsive: 0,

        //Custom selectors
        sectionSelector: '.section',
        slideSelector: '.slide',

        //events
        onLeave: function(index, nextIndex, direction){},
        afterLoad: function(anchorLink, index){},
        afterRender: function(){},
        afterResize: function(){},
        afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){},
        onSlideLeave: function(anchorLink, index, slideIndex, direction){}
      });
      
    });
  </script>
</head>
<body>
  <div id="fullpage">
    <div class="section">
      <?php include ('underConstruction.php'); ?>
    </div>
    <div class="section">
      <?php include ('contactForm.php'); ?>
    </div>
  </div>
</body>

