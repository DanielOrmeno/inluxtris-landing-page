<div id="contact" class="container contactCont">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <h1 class="headingCenter"> Get in touch </h1>
      <br>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="card">
        <div class="row">
          <h1 class="headingCenter">Lets get social</h1>
          <br>
          <div class="col-md-4 col-sm-4 col-xs-4">
            <img src="res/icons/facebookCS1.png" class="img-responsive centerImg">
          </div>
          <div class="col-md-4 col-sm-4 col-xs-4">
            <img src="res/icons/twitterCS1.png" class="img-responsive centerImg">
          </div>
          <div class="col-md-4 col-sm-4 col-xs-4">
            <img src="res/icons/googleCS1.png" class="img-responsive centerImg">
          </div>
        </div>
        <br>
        <div class="row">
          <h1 class="headingCenter">Subscribe to our newsleter</h1>
          <br>
          <div class="col-md-6 col-md-offset-2 col-sm-8 col-xs-10">
            <input type="text" class="form-control" placeholder="Email Address">
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12">
            <a href="#" class="btn btn-custom" role="button">Submit</a>
          </div>
        </div>
        <br>
      </div>
    </div>
    <div class="col-md-6 col-sm-6">
      <div class="card">
        <h1 class="headingLeft">Email us</h1>
        <div class="row">
          <div class="col-md-8 col-md-offset-3  col-sm-8 col-sm-offset-4 col-xs-10 col-xs-offset-2">
            <input type="text" class="form-control" placeholder="Full name">
          </div>
        </div> <!-- Name Row -->
        <br>
        <div class="row">
          <div class="col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-4 col-xs-10 col-xs-offset-2">
            <input type="text" class="form-control" placeholder="Email address">
          </div>
        </div> <!-- email row -->
        <br>
        <div class="row">
          <div class="col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-4 col-xs-10 col-xs-offset-2">
            <div>
              <textarea rows="10" cols="50" class="form-control" placeholder="Your thoughts"></textarea>
              <br>
              <a href="#" class="btn btn-custom" role="button">Send</a>
            </div>
            <br>
          </div>
        </div> <!-- text area row -->
      </div>
    </div>
  </div>
</div>